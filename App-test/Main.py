from flask import Flask, render_template, request, jsonify
import time
from ApiHelper import redditApi
import json
import re

app = Flask(__name__)

selected_topic = "news"
last_request_time = 0
redditData = None

@app.route('/')
def home():
    global selected_topic
    global last_request_time
    global redditData

    current_time = time.time()
    time_elapsed = current_time - last_request_time

    if time_elapsed > 60000 or redditData is None:
        fetchedRedditData = redditApi(selected_topic)
        if not fetchedRedditData is None:
            redditData = fetchedRedditData
        last_request_time = time.time()

    content = extract_titles(redditData)
    return render_template('index.html', content=content)

def extract_titles(data):
    if data is None:
        return ""
    titles = [child['data']['title'] for child in data['data']['children']]
    cleaned_titles = [re.sub(r'[\x00-\x1f\x7f-\x9f]', '', title) for title in titles]
    return " ".join(cleaned_titles)

@app.route('/select-topic', methods=['POST'])
def select_topic():
    global selected_topic
    selected_topic = request.json.get('selected_topic')
    return jsonify({'status': 'success'})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0',port=8080)
