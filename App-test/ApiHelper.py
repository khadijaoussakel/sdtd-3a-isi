import time
import requests
import json
import os

def redditApi(SUBREDDIT_NAME):
    CLIENT_ID = "lbSr8mgsLiJBIc2wITauxg"
    CLIENT_SECRET = "HyaCvTpofGx2KmFqk7_IzaH2-YSTCQ"
    AUTH_URL = "https://www.reddit.com/api/v1/access_token"
    TOKEN_FILE = "token_cache.json"

    def get_new_token():
        auth = (CLIENT_ID, CLIENT_SECRET)
        data = {
            "grant_type": "client_credentials"
        }
        response = requests.post(AUTH_URL, auth=auth, data=data)
        if response.status_code == 429:
            retry_after = int(response.headers.get('Retry-After', 1))
            print(f"Rate limit exceeded for token request. Retrying after {retry_after} seconds.")
            time.sleep(retry_after)
            return get_new_token()
        
        token_data = response.json()
        save_token_to_file(token_data)
        return token_data

    def save_token_to_file(token_data):
        token_data["timestamp"] = time.time()
        with open(TOKEN_FILE, "w") as file:
            json.dump(token_data, file)

    def get_saved_token():
        if os.path.exists(TOKEN_FILE):
            with open(TOKEN_FILE, "r") as file:
                token_data = json.load(file)
                current_time = time.time()
                if current_time - token_data["timestamp"] > token_data["expires_in"]:
                    print("Token is expired. Fetching a new one.")
                    return None
                return token_data
        return None

    token_data = get_saved_token()

    if not token_data or "access_token" not in token_data:
        token_data = get_new_token()

    access_token = token_data["access_token"]

    API_ENDPOINT = f"https://oauth.reddit.com/r/{SUBREDDIT_NAME}/top?limit=10"

    headers = {
        "Authorization": f"bearer {access_token}",
    }

    try:
        response = requests.get(API_ENDPOINT, headers=headers)

        if response.status_code == 429:
            retry_after = int(response.headers.get('Retry-After', 1))
            print(f"Rate limit exceeded. Retrying after {retry_after} seconds.")
            time.sleep(retry_after)
            return redditApi(SUBREDDIT_NAME)

        elif response.status_code != 200:
            print(
                f"Error with status code {response.status_code}: {response.text}")
            return None

        return response.json()

    except requests.exceptions.RequestException as e:
        print(f"Request failed: {e}")
        return None

    except json.JSONDecodeError as e:
        print(f"Failed to parse JSON: {e}")
        return None
