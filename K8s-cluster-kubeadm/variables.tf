variable "project_id" {
    type    = string
    default = "sdtd-3a-isi"
}
variable "region" {
    type    = string
    default = "europe-west9"
}
variable "zone" {
    type    = string
    default = "europe-west9-b"
}
variable "workers_count" {
    type = number
    default = 2
}
variable "masters_count" {
    type = number
    default = 2 
}
variable "haproxy_count" {
    type = number
    default = 2 
}

variable "ssh_username" {
    type = string
    default = "hp"
}



