resource "google_compute_network" "k8s_network" {
  name = "k8s-network"
}

resource "google_compute_subnetwork" "k8s_subnet" {
  name          = "k8s-subnet"
  network       = google_compute_network.k8s_network.name
  ip_cidr_range = "10.0.0.0/24"
}

resource "google_compute_firewall" "allow-internal" {
  name    = "allow-internal"
  network = google_compute_network.k8s_network.name
  source_ranges = ["10.0.0.0/24"]
  allow {
    protocol = "all"
  }
}

resource "google_compute_firewall" "allow-external" {
  name    = "allow-external"
  network = google_compute_network.k8s_network.name

  allow {
    protocol = "tcp"
    ports    = ["22","112","8080","80","443","6443", "2379-2380", "10250", "10251", "10252", "30000-32767", "6783","9092"]
  }
  allow {
    protocol = "icmp"
  }
  source_ranges = ["0.0.0.0/0"]
}