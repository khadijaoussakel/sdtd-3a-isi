frontend app-frontend
    bind *:80
    mode tcp
    default_backend app-backend

backend app-backend
    mode tcp
    balance roundrobin
    option tcp-check
    %{~ for worker_ip in split(" ", worker_ips) ~}
    server worker${worker_ip} ${worker_ip}:30432 check fall 3 rise 2
    %{~ endfor ~}

frontend k8s-master-frontend
    bind *:6443
    mode tcp
    option tcplog
    default_backend k8s-master-backend

backend k8s-master-backend
    mode tcp
    balance roundrobin
    option tcp-check
    %{~ for master_ip in split(" ", master_ips) ~}
    server master${master_ip} ${master_ip}:6443 check fall 3 rise 2
    %{~ endfor ~}
    server control-plane ${control_plane_ip}:6443 check fall 3 rise 2

frontend stats
    bind *:8080
    mode http
    stats enable
    stats uri /
    stats refresh 20s
    stats realm HAProxy\\ Statistics
    stats auth admin:haproxy