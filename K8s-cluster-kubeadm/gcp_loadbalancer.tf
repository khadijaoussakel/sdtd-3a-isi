resource "google_compute_global_address" "default" {
  name = "lb-ipv4-1"
}

# Health Check pour le port 80
resource "google_compute_health_check" "tcp" {
  name               = "haproxy-tcp-health-check"
  project            = var.project_id
  check_interval_sec = 5
  timeout_sec        = 5
  tcp_health_check {
    port = 80
  }
}

# Health Check pour le port 6443
resource "google_compute_health_check" "tcp_6443" {
  name               = "haproxy-tcp-6443-health-check"
  project            = var.project_id
  check_interval_sec = 5
  timeout_sec        = 5
  tcp_health_check {
    port = 6443
  }
}

# Groupe d'instances pour HAProxy
resource "google_compute_instance_group" "haproxy_group" {
  name      = "haproxy-instance-group"
  instances = [for i in google_compute_instance.load-balancer : i.id]
  zone      = var.zone
}

# Backend Service pour le port 80
resource "google_compute_backend_service" "backend_haproxy" {
  name         = "backend-haproxy"
  protocol     = "TCP"
  backend {
    group     = google_compute_instance_group.haproxy_group.id
  }
  health_checks = [google_compute_health_check.tcp.id]
}

# Backend Service pour le port 6443
resource "google_compute_backend_service" "backend_haproxy_6443" {
  name         = "backend-haproxy-6443"
  protocol     = "TCP"
  backend {
    group     = google_compute_instance_group.haproxy_group.id
  }
  health_checks = [google_compute_health_check.tcp_6443.id]
}

# Target TCP Proxy pour le port 80
resource "google_compute_target_tcp_proxy" "default" {
  name            = "tcp-lb-proxy"
  backend_service = google_compute_backend_service.backend_haproxy.id
}

# Target TCP Proxy pour le port 6443
resource "google_compute_target_tcp_proxy" "default_6443" {
  name            = "tcp-6443-lb-proxy"
  backend_service = google_compute_backend_service.backend_haproxy_6443.id
}

# Règle de forwarding globale pour le port 80
resource "google_compute_global_forwarding_rule" "default" {
  name       = "tcp-content-rule"
  target     = google_compute_target_tcp_proxy.default.id
  ip_address = google_compute_global_address.default.address
  port_range = "80"
}

# Règle de forwarding globale pour le port 6443
resource "google_compute_global_forwarding_rule" "default_6443" {
  name       = "tcp-6443-content-rule"
  target     = google_compute_target_tcp_proxy.default_6443.id
  ip_address = google_compute_global_address.default.address
  port_range = "6443"
}