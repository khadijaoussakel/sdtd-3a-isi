resource "google_compute_disk" "disk_sdtd" {
  count   = var.workers_count
  name    = "disk-sdtd-${count.index}"
  type    = "pd-standard"
  zone    = var.zone
  size    = 70
}

resource "google_compute_attached_disk" "attach_disk_to_workers" {
  count    = var.workers_count
  disk     = google_compute_disk.disk_sdtd[count.index].self_link
  instance = google_compute_instance.k8s-worker[count.index].name
  mode     = "READ_WRITE"
  zone     = var.zone
}

resource "null_resource" "configure_workers" {
  depends_on = [
  google_compute_attached_disk.attach_disk_to_workers,
  null_resource.playbooks,
  ]
  count = var.workers_count
  
  connection {
    type        = "ssh"
    host        = google_compute_instance.k8s-worker[count.index].network_interface[0].access_config[0].nat_ip
    user        = var.ssh_username
    private_key = tls_private_key.sdtd.private_key_pem
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /mnt/sdtd",
      "sudo mkfs.ext4 /dev/sdb",
      "sudo mount /dev/sdb /mnt/sdtd",
      "sudo sed -i 's|root = \"/var/lib/containerd\"|root = \"/mnt/sdtd\"|g' /etc/containerd/config.toml",
      "sudo systemctl restart containerd",
    ]
  }
}
