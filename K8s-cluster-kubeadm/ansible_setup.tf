resource "google_service_account" "service_account" {
  account_id   = "ansible-server"
  display_name = "ansible-server"
}
resource "google_service_account_key" "service_account" {
  service_account_id = google_service_account.service_account.name
  public_key_type    = "TYPE_X509_PEM_FILE"
}
resource "local_file" "service_account" {
    content  = base64decode(google_service_account_key.service_account.private_key)
    filename = "./ansible-config/service_account.json"
}
resource "google_project_iam_binding" "project" {
  project = "sdtd-3a-isi"
  role    = "roles/compute.admin"
  members = [
    "serviceAccount:${google_service_account.service_account.email}",
  ]
}
resource "google_project_iam_member" "network_viewer" {
  project = "sdtd-3a-isi"
  role    = "roles/compute.networkViewer"
  member  = "serviceAccount:${google_service_account.service_account.email}"
}

resource "google_compute_instance" "ansible-server" {
  name         = "ansible-server"
  machine_type = "e2-medium"
  zone         = "europe-west9-b"
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }
  network_interface {
    network    = google_compute_network.k8s_network.name
    subnetwork = google_compute_subnetwork.k8s_subnet.name
    access_config {
      
    }
  }

  metadata={
    ssh-keys = "${var.ssh_username}:${tls_private_key.sdtd.public_key_openssh}"
  }
    connection {
    type        = "ssh"
    host        = self.network_interface[0].access_config[0].nat_ip
    user        = var.ssh_username
    private_key = tls_private_key.sdtd.private_key_pem
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get upgrade -y",
      "sudo apt-get update",
      "sudo apt-get install software-properties-common -y",
      "sudo apt-add-repository --yes --update ppa:ansible/ansible",
      "sudo apt-get update",
      "sudo apt-get -y install python",
      "sudo apt-get install ansible -y",
      "sudo apt-get -y install python3-pip",
      "sudo pip install google-auth requests",
      "ssh-keygen -t rsa -b 2048 -f .ssh/id_rsa -N ''",
      "mkdir -p ~/sdtd",
      "echo Done!"
    ]
  }

provisioner "local-exec" {
  command =" scp -o StrictHostKeyChecking=no -i ${path.module}/sdtd_private_key.pem -r ./ansible-config ${var.ssh_username}@${self.network_interface[0].access_config[0].nat_ip}:~/sdtd/"
}

}

resource "null_resource" "copy_ssh_key" {
  triggers = {
    ansible_instance_ip = google_compute_instance.ansible-server.network_interface[0].access_config[0].nat_ip
  }
 
  provisioner "local-exec" {
    command = <<EOT
      scp -o StrictHostKeyChecking=no -i ${path.module}/sdtd_private_key.pem ${var.ssh_username}@${self.triggers.ansible_instance_ip}:~/.ssh/id_rsa.pub ./
    EOT
  }
}
resource "null_resource" "playbooks" {
  triggers = {
      control_plane_ip = google_compute_instance.k8s-control-plane.network_interface[0].access_config[0].nat_ip,
    }
    depends_on = [
    google_compute_instance.k8s-control-plane,
    google_compute_instance.k8s-master,
    google_compute_instance.k8s-worker,
    google_compute_instance.load-balancer,
    google_compute_instance.ansible-server,

  ]
    connection {
    type        = "ssh"
    host        = google_compute_instance.ansible-server.network_interface[0].access_config[0].nat_ip
    user        = var.ssh_username
    private_key = tls_private_key.sdtd.private_key_pem
  }
  provisioner "remote-exec" {
    inline = [
      "cd ~/sdtd/ansible-config",
      "ansible-inventory --list",
      "ansible-playbook setup_control_plane.yml",
      "ansible-playbook setup_other_nodes.yml",
      "ansible-playbook join_masters.yml",
      "ansible-playbook join_workers.yml",
    ]
    }
}