provider "tls" {
  # Pas de configuration spécifique nécessaire pour tls
}

resource "tls_private_key" "sdtd" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "private_key" {
  content  = tls_private_key.sdtd.private_key_pem
  filename = "${path.module}/sdtd_private_key.pem"
  file_permission = "0600"
}

resource "local_file" "public_key" {
  content  = tls_private_key.sdtd.public_key_openssh
  filename = "${path.module}/sdtd_public_key.pub"
  file_permission = "0644"
}
