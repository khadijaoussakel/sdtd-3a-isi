data "template_file" "haproxy_cfg" {
  template = file("haproxy.cfg.tpl")

   vars = {
    worker_ips       = join(" ", [for worker in google_compute_instance.k8s-worker : worker.network_interface[0].access_config[0].nat_ip])
    master_ips       = join(" ", [for master in google_compute_instance.k8s-master : master.network_interface[0].access_config[0].nat_ip])
    control_plane_ip = google_compute_instance.k8s-control-plane.network_interface[0].access_config[0].nat_ip
  }
}

resource "google_compute_instance" "load-balancer" {
  count        = var.haproxy_count
  name         = "load-balancer-${count.index}"
  machine_type = "e2-medium"
  zone         = var.zone
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }
  network_interface {
    network    = google_compute_network.k8s_network.name
    subnetwork = google_compute_subnetwork.k8s_subnet.name
    access_config {
      
    }
  }
  metadata={
    ssh-keys = "${var.ssh_username}:${tls_private_key.sdtd.public_key_openssh}"
  }
  depends_on = [
    google_compute_instance.k8s-control-plane,
    google_compute_instance.k8s-master,
    google_compute_instance.k8s-worker,
  ]
   connection {
    type        = "ssh"
    host        = self.network_interface[0].access_config[0].nat_ip
    user        = var.ssh_username
    private_key = tls_private_key.sdtd.private_key_pem
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y software-properties-common",
      "sudo add-apt-repository ppa:vbernat/haproxy-2.1 --yes",
      "sudo apt-get update",
      "sudo apt-cache policy haproxy",
      "sudo apt install -y haproxy",
      "echo '${data.template_file.haproxy_cfg.rendered}' | sudo tee /etc/haproxy/haproxy.cfg",
      "sleep 5",
      "sudo service haproxy restart",
   ]
  }

}

