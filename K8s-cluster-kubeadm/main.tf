resource "google_service_account" "k8s_node" {
  account_id   = "k8s-node"
  display_name = "Kubernetes-node"
}
resource "google_project_iam_member" "k8s_node_storage_admin" {
  project = var.project_id
  role    = "roles/compute.storageAdmin"
  member  = "serviceAccount:${google_service_account.k8s_node.email}"
}

resource "google_compute_instance" "k8s-control-plane" {
  name         = "k8s-control-plane"
  machine_type = "e2-medium"
  zone         = var.zone
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }
  network_interface {
    network    = google_compute_network.k8s_network.name
    subnetwork = google_compute_subnetwork.k8s_subnet.name
    access_config {
      
    }
  }
  service_account {
    email  = google_service_account.k8s_node.email
    scopes = ["cloud-platform"]
  }
  metadata={
    ssh-keys = "${var.ssh_username}:${tls_private_key.sdtd.public_key_openssh}"
  }
  depends_on = [
    null_resource.copy_ssh_key
  ]
   connection {
    type        = "ssh"
    host        = self.network_interface[0].access_config[0].nat_ip
    user        = var.ssh_username
    private_key = tls_private_key.sdtd.private_key_pem
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo apt-get install -y openssh-server",
    ]
  }
  
  provisioner "local-exec" {
    command = "scp -o StrictHostKeyChecking=no -i ${path.module}/sdtd_private_key.pem id_rsa.pub ${var.ssh_username}@${self.network_interface[0].access_config[0].nat_ip}:~/.ssh/id_rsa.pub"
  }
  provisioner "remote-exec" {
    inline = [
      "cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys",
    ]
  }
}

resource "google_compute_instance" "k8s-master" {
  count        = var.masters_count
  name         = "k8s-master-${count.index}"
  machine_type = "e2-medium"
  zone         = var.zone
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }
  network_interface {
    network    = google_compute_network.k8s_network.name
    subnetwork = google_compute_subnetwork.k8s_subnet.name
    access_config {
      
    }
  }
  service_account {
    email  = google_service_account.k8s_node.email
    scopes = ["cloud-platform"]
  }
  metadata={
    ssh-keys = "${var.ssh_username}:${tls_private_key.sdtd.public_key_openssh}"
  }
  depends_on = [
    null_resource.copy_ssh_key
  ]
   connection {
    type        = "ssh"
    host        = self.network_interface[0].access_config[0].nat_ip
    user        = var.ssh_username
    private_key = tls_private_key.sdtd.private_key_pem
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo apt-get install -y openssh-server",
    ]
  }
  
  provisioner "local-exec" {
    command = "scp -o StrictHostKeyChecking=no -i ${path.module}/sdtd_private_key.pem id_rsa.pub ${var.ssh_username}@${self.network_interface[0].access_config[0].nat_ip}:~/.ssh/id_rsa.pub"
  }
  provisioner "remote-exec" {
    inline = [
      "cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys",
    ]
  }
}

resource "google_compute_instance" "k8s-worker" {
  count        = var.workers_count
  name         = "k8s-worker-${count.index}"
  machine_type = "e2-medium"
  zone         = var.zone
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }
  network_interface {
    network    = google_compute_network.k8s_network.name
    subnetwork = google_compute_subnetwork.k8s_subnet.name
    access_config {
      
    }
  }
  service_account {
    email  = google_service_account.k8s_node.email
    scopes = ["cloud-platform"]
  }
  depends_on = [
    google_compute_instance.k8s-master,
    null_resource.copy_ssh_key
  ]

  metadata={
    ssh-keys = "${var.ssh_username}:${tls_private_key.sdtd.public_key_openssh}"
  }
  connection {
    type        = "ssh"
    host        = self.network_interface[0].access_config[0].nat_ip
    user        = var.ssh_username
    private_key = tls_private_key.sdtd.private_key_pem
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update -y",
      "sudo apt-get install -y openssh-server",
    ]
  }
  provisioner "local-exec" {
  command = "scp -o StrictHostKeyChecking=no -i ${path.module}/sdtd_private_key.pem id_rsa.pub ${var.ssh_username}@${self.network_interface[0].access_config[0].nat_ip}:~/.ssh/id_rsa.pub"
}
  provisioner "remote-exec" {
    inline = [
      "cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys",
      
    ]
  }

}



