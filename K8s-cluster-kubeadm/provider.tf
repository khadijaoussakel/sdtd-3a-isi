provider "google" {
  credentials = file("sdtd-3a-isi-d26064fb032d.json")
  project = var.project_id
  region  = var.region
}