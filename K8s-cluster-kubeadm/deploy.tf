resource "google_service_account" "gcr_service_account" {
  account_id   = "gcr-service-account"
  display_name = "gcr-service-account"
}
resource "google_service_account_key" "gcr_service_account_key" {
  service_account_id = google_service_account.gcr_service_account.name
}
resource "local_file" "gcr_service_account" {
    content  = base64decode(google_service_account_key.gcr_service_account_key.private_key)
    filename = "../Deployments/gcr_service_account.json"
}
resource "google_project_iam_binding" "gcr_project" {
  project = "sdtd-3a-isi"
  role    = "roles/storage.objectViewer"
  members = [
    "serviceAccount:${google_service_account.gcr_service_account.email}",
  ]
}
resource "null_resource" "deployment" {
    depends_on=[
     null_resource.playbooks,
     google_service_account.gcr_service_account,
     null_resource.configure_workers,
    ]
    connection {
    type        = "ssh"
    host        = google_compute_instance.k8s-control-plane.network_interface[0].access_config[0].nat_ip
    user        = var.ssh_username
    private_key = tls_private_key.sdtd.private_key_pem
  }
  provisioner "local-exec" {
  command =" scp -o StrictHostKeyChecking=no -i ${path.module}/sdtd_private_key.pem -r ../Deployments ${var.ssh_username}@${google_compute_instance.k8s-control-plane.network_interface[0].access_config[0].nat_ip}:~/sdtd/"
  }
  provisioner "remote-exec" {
    inline = [
      "cd ~/sdtd/",
      "sudo kubectl create secret docker-registry gcr-secret --docker-server=gcr.io --docker-username=_json_key --docker-password=\"$(cat gcr_service_account.json)\"",
      "sudo kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml",
      "sudo kubectl patch deployment metrics-server -n kube-system --type='json' -p='[{\"op\": \"add\", \"path\": \"/spec/template/spec/containers/0/args/-\", \"value\": \"--kubelet-insecure-tls\"}]'",
      "sudo kubectl apply -f storage-class.yaml",
      "sudo kubectl apply -f persistent-volume.yaml",
      "sudo kubectl apply -f api-config.yaml",
      "sudo kubectl apply -f kafka.yaml",
      "sudo kubectl apply -f flask-server.yaml",
      "sudo kubectl apply -f model-worker.yaml",
      "sudo kubectl apply -f bdd-server.yaml",
      "sudo kubectl apply -f prom.yaml",
      "sleep 200",
      "sudo kubectl apply -f hpa.yaml",

  ]
    }
}