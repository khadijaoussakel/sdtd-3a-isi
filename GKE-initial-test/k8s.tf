resource "google_container_cluster" "sdtd-gke-cluster" {
  name     = var.cluster_name
  location = "europe-west9-b"
  deletion_protection = false
  remove_default_node_pool = true
  initial_node_count       = 1
}

resource "google_container_node_pool" "primary_nodes" {
  name       = "node-pool"
  location   = "europe-west9-b"
  cluster    = google_container_cluster.sdtd-gke-cluster.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-standard-2"
     metadata = {
      disable-legacy-endpoints = "true"
    }

  }

  }

