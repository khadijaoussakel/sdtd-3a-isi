variable "project_id" {
    type    = string
    default = "sdtd-3a-isi"
}
variable "region" {
    type    = string
    default = "europe-west9"
}
variable "cluster_name" {
    type    = string
    default = "sdtd-gke-cluster"
}
