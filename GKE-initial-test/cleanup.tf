# To destroy the GKE cluster
resource "google_container_cluster" "cleanup" {
  name     = var.cluster_name
  location = "europe-west9-b"

  depends_on = [google_container_cluster.sdtd-gke-cluster]
  count = var.destroy_cluster ? 1 : 0
}

variable "destroy_cluster" {
  type    = bool
  default = false
}

# terraform apply -var "destroy_cluster=true"


