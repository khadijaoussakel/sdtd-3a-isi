import socket
import pickle


def client_worker(host, port, param):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        print(f"Connecting to server at {host}:{port}")
        s.connect((host, port))
        print(f"Sending parameters: {param}")

        # Send data
        s.sendall(param.encode())

        # Wait and receive the response
        print("Waiting for server response...")

        while True:
            data = s.recv(1024)
            if data:
                result = pickle.loads(data)
                print(f"Received result: {result}")
                # break
    finally:
        print("Closing the connection.")
        s.close()


if __name__ == "__main__":
    HOST = "34.144.216.36"
    PORT = 80
    param = "N:20,num_clusters:4"
    print("Starting client worker...")
    client_worker(HOST, PORT, param)
