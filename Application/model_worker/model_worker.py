from confluent_kafka import Producer, Consumer, KafkaError
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from sentence_transformers import SentenceTransformer
import numpy as np
import pandas as pd
import requests
import pickle
import os


model = SentenceTransformer("paraphrase-MiniLM-L6-v2")


def parse_config(input_string):
    pairs = input_string.split(",")
    config = {}
    for pair in pairs:
        key, value = pair.split(":")
        key = key.strip()
        config[key] = int(value.strip())
    print("new_config : ", config)
    return config


def process(texts, config):
    print("start processing")
    embeddings = model.encode(texts)
    results = []
    if len(embeddings) <= 1:
        return results

    num_clusters = min(config["num_clusters"], len(embeddings))
    kmeans = KMeans(n_clusters=num_clusters, random_state=0, n_init=10)
    cluster_indices = kmeans.fit_predict(embeddings)

    # Create a DataFrame to store texts and their cluster assignments
    df = pd.DataFrame({"Sentence": texts, "Cluster": cluster_indices})

    # Store the texts in each cluster
    cluster_texts = {}
    for cluster_id in range(num_clusters):
        cluster_data = df[df["Cluster"] == cluster_id]["Sentence"].tolist()
        cluster_texts[cluster_id] = cluster_data

    # Calculate TF-IDF scores for each cluster
    vectorizer = TfidfVectorizer(stop_words="english")
    for cluster_id, cluster_data in cluster_texts.items():
        if cluster_data:
            try:
                tfidf_matrix = vectorizer.fit_transform(cluster_data)
                sum_tfidf = np.sum(tfidf_matrix, axis=0)
                features = np.array(vectorizer.get_feature_names_out())

                sorted_indices = np.argsort(sum_tfidf)[0, ::-1]
                sorted_terms = features[sorted_indices][0]
                for i, item in enumerate(sorted_terms[:4]):
                    results.append(
                        (
                            len(embeddings),
                            item,
                            i,
                            len(cluster_data),
                        )
                    )
            except ValueError:
                # Handle the empty vocabulary case
                print(f"Empty vocabulary for cluster {cluster_id}. Skipping.")
                continue

    print("end processing")
    return results


producer = Producer(
    {"bootstrap.servers": f"{os.getenv('KAFKA_API', 'kafka-server')}:9092"}
)
configConsumer = Consumer(
    {
        "bootstrap.servers": f"{os.getenv('KAFKA_API', 'kafka-server')}:9092",
        "group.id": "config",
        "auto.offset.reset": "earliest",
    }
)

configConsumer.subscribe(["config"])


def task(config):
    bdd_api = os.environ.get("BDD_SERVER_API", "localhost")
    if not bdd_api:
        raise ValueError("KAFKA_API environment variable is not set")
    # coontinuation token
    response = requests.get(f"http://{bdd_api}:5000/getdata?token={config['token']}")
    bddResponse = response.json()
    texts = bddResponse["data"]
    results = process(texts, config)
    return results, bddResponse["token"]


while True:
    msg = configConsumer.poll(0.1)
    if msg is None:
        continue
    if msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            print("Reached end of partition")
        else:
            print(f"Error while consuming: {msg.error()}")
    else:
        config_msg = pickle.loads(msg.value())
        print(config_msg)
        config = parse_config(config_msg["param"])
        result = {}
        print(config_msg)

        result["addr"] = config_msg["addr"]
        res = task(config)
        result["result"] = res[0]
        result["token"] = res[1]
        producer.produce("result", value=pickle.dumps(result))
