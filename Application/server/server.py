import socket
import threading
import pickle
from confluent_kafka import Producer, Consumer
import os

# Global dictionary for client sockets and parameters
client_sockets = {}
params_dict = {}

lock = threading.Lock()

default_config = "N:10,num_clusters:2,token:0"
# Kafka configuration
kafka_config = {
    "bootstrap.servers": f"{os.getenv('KAFKA_API', 'kafka-server')}:9092",
    "group.id": "server-group",
    "auto.offset.reset": "earliest",
}

# Kafka topics
params_topic = "config"
results_topic = "result"

producer = Producer(kafka_config)


def parse_config(input_string):
    pairs = input_string.split(",")
    config = {}
    for pair in pairs:
        key, value = pair.split(":")
        key = key.strip()
        config[key] = int(value.strip())
    return config


def config_to_string(config_dict):
    return ",".join([f"{key}:{value}" for key, value in config_dict.items()])


# Thread 1: Connection Listener
def connection_listener(host, port):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen()
    print("Connection listener started. Waiting for incoming connections...")

    while True:
        client_socket, addr = server_socket.accept()
        client_socket.setblocking(False)  # Set the socket to non-blocking
        print(f"New connection established from {addr}")

        with lock:
            client_sockets[addr] = client_socket
            params_dict[addr] = default_config
            msg_produced = {"addr": addr, "param": default_config}
            print(f"Producing to Kafka topic: {msg_produced} for client {addr}")
            producer.produce(params_topic, value=pickle.dumps(msg_produced))


# Thread 2: Data Receiver
def data_receiver():
    print("Data receiver thread started. Waiting for data from clients...")
    while True:
        with lock:
            for addr, client_socket in list(client_sockets.items()):
                try:
                    data = client_socket.recv(1024)
                    if data:
                        param_dict_parsed = parse_config(params_dict[addr])
                        new_params = data.decode("utf-8")
                        print(f"Received data from {addr}: {new_params }")
                        new_params_dict = parse_config(new_params)
                        new_params_dict["token"] = param_dict_parsed["token"]
                        updated_config = config_to_string(new_params_dict)
                        print(f"new config for {addr} are: {updated_config}")
                        params_dict[addr] = updated_config
                    else:
                        # Socket is disconnected, remove it from the dict
                        print(f"Client {addr} has disconnected.")
                        client_socket.close()
                        del client_sockets[addr]
                        del params_dict[addr]
                except BlockingIOError:
                    # No data available yet from this client
                    pass
                except Exception as e:
                    print(f"Error with client {addr}: {e}")
                    client_socket.close()
                    del client_sockets[addr]
                    del params_dict[addr]


# Thread 3: Kafka Consumer
def kafka_consumer():
    consumer = Consumer(kafka_config)
    consumer.subscribe([results_topic])
    print("Kafka consumer thread started. Consuming data from Kafka topic...")

    while True:
        msg = consumer.poll(1.0)
        if msg is None or msg.error():
            continue

        result = pickle.loads(msg.value())
        with lock:
            addr = result["addr"]
            texts = result["result"]
            continuationToken = result["token"]
            if addr in client_sockets:
                try:
                    client_socket = client_sockets[addr]
                    print(f"Sending result to client {addr}: {texts}")
                    client_socket.sendall(pickle.dumps(result["result"]))

                    param_dict_parsed = parse_config(params_dict[addr])
                    param_dict_parsed["token"] = continuationToken
                    updated_config = config_to_string(param_dict_parsed)
                    params_dict[addr] = updated_config
                    print(
                        f"The continuation token for the client {addr} has been updated {params_dict[addr]}"
                    )

                    msg_produced = {"addr": addr, "param": params_dict[addr]}
                    print(f"Producing to Kafka topic: {msg_produced} for client {addr}")
                    producer.produce(params_topic, value=pickle.dumps(msg_produced))
                except Exception as e:
                    print(f"Erreur avec le client {addr}: {e}")


if __name__ == "__main__":
    HOST = "0.0.0.0"
    print(HOST)
    PORT = 65432

    print("Starting server...")
    threading.Thread(target=connection_listener, args=(HOST, PORT)).start()
    threading.Thread(target=data_receiver).start()
    threading.Thread(target=kafka_consumer).start()
