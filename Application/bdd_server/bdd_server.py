from flask import Flask, jsonify, request

app = Flask(__name__)

file_path = "dataset.txt"
file = open(file_path, "r", encoding="utf-8")
data = file.readlines()


@app.route("/getdata", methods=["GET"])
def send_data():
    token = request.args.get("token", default=0, type=int)
    subset_data = data[token : token + 10]

    # Calculate the next token
    next_token = token + 3
    if next_token >= len(data):
        next_token = 0

    return jsonify({"data": subset_data, "token": next_token})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
