#!/bin/sh

# Start Zookeeper and Kafka in the background
zookeeper-server-start.sh /opt/kafka/config/zookeeper.properties &
kafka-server-start.sh /opt/kafka/config/server.properties &
kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic result 
kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic config 

# Keep the container running
tail -f /dev/null