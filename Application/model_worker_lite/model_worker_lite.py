from confluent_kafka import Producer, Consumer, KafkaError
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import pandas as pd
import requests
import pickle
import os


def parse_config(input_string):
    pairs = input_string.split(",")
    config = {}
    for pair in pairs:
        key, value = pair.split(":")
        key = key.strip()
        config[key] = int(value.strip())
    print("new_config : ", config)
    return config


def process(texts, config):
    print("start processing")
    results = []
    if len(texts) <= 1:
        return results

    # Step 1: Use TF-IDF vectorization as the feature representation for clustering
    vectorizer = TfidfVectorizer(stop_words="english")
    tfidf_matrix = vectorizer.fit_transform(texts)

    num_clusters = min(config["num_clusters"], len(texts))
    kmeans = KMeans(n_clusters=num_clusters, random_state=0, n_init=10)
    cluster_indices = kmeans.fit_predict(tfidf_matrix)

    # Create a DataFrame to store texts and their cluster assignments
    df = pd.DataFrame({"Sentence": texts, "Cluster": cluster_indices})

    # Step 2: For each cluster, find the most important words
    for cluster_id in range(num_clusters):
        cluster_data = df[df["Cluster"] == cluster_id]["Sentence"].tolist()
        if not cluster_data:
            continue

        # Calculate TF-IDF for the current cluster
        tfidf_matrix_cluster = vectorizer.transform(cluster_data)
        sum_tfidf = np.sum(tfidf_matrix_cluster, axis=0).A1  # Flatten the array
        features = np.array(vectorizer.get_feature_names_out())

        sorted_indices = np.argsort(sum_tfidf)[::-1]  # Descending order of importance
        sorted_terms = features[sorted_indices][:4]  # Top 4 terms

        for i, item in enumerate(sorted_terms):
            results.append(
                (
                    len(texts),
                    item,
                    i,
                    len(cluster_data),
                )
            )

    print("end processing")
    return results


producer = Producer(
    {"bootstrap.servers": f"{os.getenv('KAFKA_API', 'kafka-server')}:9092"}
)
configConsumer = Consumer(
    {
        "bootstrap.servers": f"{os.getenv('KAFKA_API', 'kafka-server')}:9092",
        "group.id": "config",
        "auto.offset.reset": "earliest",
    }
)

configConsumer.subscribe(["config"])


def task(config):
    bdd_api = os.getenv("BDD_SERVER_API", "kafka-server")

    response = requests.get(f"http://{bdd_api}:5000/getdata")
    texts = response.json()
    results = process(texts, config)
    return results


while True:
    msg = configConsumer.poll(0.1)
    if msg is None:
        continue
    if msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            print("Reached end of partition")
        else:
            print(f"Error while consuming: {msg.error()}")
    else:
        config_msg = pickle.loads(msg.value())
        print(config_msg)
        config = parse_config(config_msg["param"])
        result = {}
        print(config_msg)
        result["addr"] = config_msg["addr"]
        result["result"] = task(config)
        producer.produce("result", value=pickle.dumps(result))
