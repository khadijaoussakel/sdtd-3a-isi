# Guide de déploiement de l'infrastructure Kubernetes sur GCP

Ce guide explique comment déployer notre infrastructure Kubernetes sur Google Cloud Platform (GCP)


## Étapes de déploiement

### 1. Création d'un compte de service pour Terraform

Créez un compte de service dans GCP qui sera utilisé par Terraform pour gérer les ressources.


### 2. Configuration du provider Terraform

Configurez le provider `google` en utilisant la clé du compte de service téléchargée.

1. Déplacez le fichier de clé JSON téléchargé dans le dossier "K8s_cluster_kubeadm"
2. Dans le fichier de configuration Terraform `provider.tf`, remplacez `<NOM_DU_FICHIER>` par le nom de votre fichier de clé JSON :

    ```hcl
    provider "google" {
      credentials = file("<NOM_DU_FICHIER>.json")
      project = var.project_id
      region  = var.region
    }
    ```
Dans le fichier variables.tf, modifiez les variables "project_id" et "region" si vous n'utilisez pas les memes

### 3. Configuration du backend pour l'état Terraform

Utilisez Google Cloud Storage (GCS) pour stocker l'état de Terraform de manière sécurisée et centralisée.

1. Créez un bucket dans GCP :


2. Configurez le backend dans le fichier de configuration Terraform backend.tf, remplacez le bucket par le nom de votre bucket :

    ```hcl
    terraform {
      backend "gcs" {
        bucket = "terraform_state_sdtd"
      }
    }
    ```

### 4. Déploiement de l'infrastructure

1. Naviguez vers le dossier :

    ```bash
    cd K8s_cluster_kubeadm
    ```

2. Initialisez Terraform :

    ```bash
    terraform init
    ```

3. Appliquez la configuration Terraform pour déployer l'infrastructure :

    ```bash
    terraform apply
    ```

### 5. Test de l'application déployée

1. Ouvrez le terminal et naviguez vers le dossier Application/client
2. Avant de lancer le script client_worker.py, vous devez configurer l'adresse IP de votre load balancer GCP dans le script. Ouvrez le fichier client_worker.py et recherchez la variable HOST et remplacez sa valeur par l'adresse IP externe de votre load balancer GCP.